/* MQ2AutoForage

   Simple plugin to automate the tast of foraging.

   Syntax:

   /startforage         - commence autoforaging.
   /stopforage          - stop autoforaging.
   /keepitem {item}     - add/change the item in the .ini file to auto-keep.
   /destroyitem {item}  - add/change the item in the .ini file to auto-destroy.

*/

#include "../MQ2Plugin.h"

PreSetup("MQ2AutoForage"); PLUGIN_VERSION(1.5);

void StartForageCommand(PSPAWNINFO pChar, PCHAR szLine);
void StopForageCommand(PSPAWNINFO pChar, PCHAR szLine);
void KeepItemCommand(PSPAWNINFO pChar, PCHAR szLine);
void DestroyItemCommand(PSPAWNINFO pChar, PCHAR szLine);
bool CheckAbilityReady(PCHAR szSkillName);
void HandleItem(PCHARINFO pCharInfo);
void Load_INI(VOID);
bool Check_INI(VOID);

bool IsForaging=false;
bool HasForaged=false;
bool ForageSuccess=false;
bool KeepDestroy=false;
bool KeepItem=false;
bool WasSitting=false;
bool AutoKeepEnabled=true;
bool AutoAddEnabled=true;
bool MQ2ForageEnabled=false;

// Added by Jaq -- Ripped off from mq2MoveUtils
bool IsBardClass(void);

PLUGIN_API VOID InitializePlugin(VOID) {
   AddCommand("/startforage",StartForageCommand);
   AddCommand("/stopforage",StopForageCommand);
   AddCommand("/keepitem",KeepItemCommand);
   AddCommand("/destroyitem",DestroyItemCommand);

   if (MQ2Globals::gGameState==GAMESTATE_INGAME) {
      if (GetCharInfo()) {
         sprintf(INIFileName,"%s\\MQ2Forage_%s_%s.ini",gszINIPath,GetCharInfo()->Name,EQADDR_SERVERNAME);
         Load_INI();
         MQ2ForageEnabled=true;
      }
   } else MQ2ForageEnabled=false;
}

PLUGIN_API VOID ShutdownPlugin(VOID) {
   RemoveCommand("/startforage");
   RemoveCommand("/stopforage");
   RemoveCommand("/keepitem");
   RemoveCommand("/destroyitem");
}

PLUGIN_API VOID OnZoned(VOID) { Load_INI(); }

PLUGIN_API VOID OnPulse(VOID) {

   if (!MQ2ForageEnabled) {
      return;
   }

   PSPAWNINFO pChSpawn = GetCharInfo()->pSpawn;
   PCHARINFO pCharInfo = NULL;

   if ((IsForaging) && !(*EQADDR_ATTACK > 0) && !(PCSIDLWND)pSpellBookWnd->dShow && !(PCSIDLWND)pGiveWnd->dShow && !(PCSIDLWND)pBankWnd->dShow && !(PCSIDLWND)pMerchantWnd->dShow && !(PCSIDLWND)pTradeWnd->dShow && !(PCSIDLWND)pLootWnd->dShow && !GetCharInfo()->pSpawn->Mount) {
   //if ((IsForaging) && !(*EQADDR_ATTACK > 0)) {
      if (CheckAbilityReady("Forage")) {
         if (pChSpawn->StandState == STANDSTATE_SIT) {
            DoCommand(pChSpawn, "/stand");
            WasSitting=true;
         } else if (((PSPAWNINFO)pLocalPlayer)->CastingData.SpellETA == 0 || (IsBardClass())) {
            HasForaged=true;
            DoAbility(pChSpawn,"Forage");
         }
      }
   }

   if (ForageSuccess) {
      if (NULL == (pCharInfo = GetCharInfo())) return;

      if (GetCharInfo2()->pInventoryArray->Inventory.Cursor) {
         ForageSuccess=false;
         HandleItem(pCharInfo);
      }
   }

   if (!ForageSuccess && KeepDestroy) {
      if (NULL == (pCharInfo = GetCharInfo())) return;

      if (GetCharInfo2()->pInventoryArray->Inventory.Cursor && KeepItem) {
         DoCommand(pChSpawn, "/autoinventory");
      } else if (GetCharInfo2()->pInventoryArray->Inventory.Cursor && !KeepItem) {
         DoCommand(pChSpawn, "/destroy");
      } else if (!GetCharInfo2()->pInventoryArray->Inventory.Cursor) {
         KeepDestroy=false;
         KeepItem=false;
         HasForaged=false;
      }
      if (WasSitting) {
         WasSitting=false;
         DoCommand(pChSpawn, "/sit");
      }
   }
}

PLUGIN_API DWORD OnIncomingChat(PCHAR Line, DWORD Color) {

   if (HasForaged && (strstr(Line, "You have scrounged up") || strstr(Line,"Your forage mastery has enabled you to find something else!")) ) {
      ForageSuccess=true;
   } else if (HasForaged && strstr(Line, "You fail to locate")) {
      ForageSuccess=false;
      HasForaged=false;
      KeepItem=false;
      KeepDestroy=false;
   }
   return 0;
}


PLUGIN_API void SetGameState(DWORD GameState) {
   if (GameState==GAMESTATE_INGAME) {
      if (GetCharInfo()) {
         sprintf(INIFileName,"%s\\MQ2Forage_%s_%s.ini",gszINIPath,GetCharInfo()->Name,EQADDR_SERVERNAME);
         Load_INI();
         MQ2ForageEnabled=true;
      }
   } else {
      MQ2ForageEnabled=false;
   }
}

void StartForageCommand(PSPAWNINFO pChar, PCHAR szLine) {
   if (MQ2ForageEnabled) {
      WriteChatColor("Auto-Forage Enabled",CONCOLOR_GREEN);
      IsForaging=true;
   }
}

void StopForageCommand(PSPAWNINFO pChar, PCHAR szLine) {
   if (MQ2ForageEnabled) {
      WriteChatColor("Auto-Forage Disabled",CONCOLOR_RED);
      IsForaging=false;
   }
}

void KeepItemCommand(PSPAWNINFO pChar, PCHAR szLine) {
   char szZone[64];
   char szMsg[MAX_STRING];
   PCHARINFO pCharInfo = GetCharInfo();

   if (MQ2ForageEnabled) {
      sprintf(szMsg, "Now auto-keeping item: %s", szLine);
      WriteChatColor(szMsg, CONCOLOR_YELLOW);
      sprintf(szZone,"%s",GetFullZone(pCharInfo->zoneId));
      WritePrivateProfileString(szZone,szLine,"keep",INIFileName);
   }
}

void DestroyItemCommand(PSPAWNINFO pChar, PCHAR szLine) {
   char szZone[64];
   char szMsg[MAX_STRING];
   PCHARINFO pCharInfo = GetCharInfo();

   if (MQ2ForageEnabled) {
      sprintf(szMsg, "Now auto-destroying item: %s", szLine);
      WriteChatColor(szMsg, CONCOLOR_YELLOW);
      sprintf(szZone,"%s",GetFullZone(pCharInfo->zoneId));
      WritePrivateProfileString(szZone,szLine,"destroy",INIFileName);
   }
}
bool CheckAbilityReady(PCHAR szSkillName) {
   for (DWORD nSkill=0;szSkills[nSkill];nSkill++) {
      if (!stricmp(szSkillName,szSkills[nSkill])) {
         if (GetCharInfo2()->Skill[nSkill]>252) {
            return false;
         }
         for (DWORD nAbility=0;nAbility<10;nAbility++) {
            if (EQADDR_DOABILITYLIST[nAbility] == nSkill) {
               if (nAbility<4) {
                  nAbility+=7;
               } else {
                  nAbility-=3;
               }

              // if (pSkillMgr->pSkill[nSkill]->AltTimer==2) {
				 if (pSkillMgr->pSkill[nSkill]->SkillCombatType==2) {
                  return gbAltTimerReady?true:false;
               } else {
                  return pCSkillMgr->IsAvailable(nSkill)?true:false;
               }
            }
         }
      }
   }
   return false;
}

void HandleItem(PCHARINFO pCharInfo) {
   
   CHAR szMsg[MAX_STRING] = {0};
   CHAR szItem[64] = {0};
   
   PSPAWNINFO pChSpawn = GetCharInfo()->pSpawn;
   PITEMINFO pCursor = GetItemFromContents(GetCharInfo2()->pInventoryArray->Inventory.Cursor);
   sprintf(szItem,"%s",pCursor->Name);

   KeepDestroy=true;

   KeepItem=Check_INI();

   if (KeepItem) {
      sprintf(szMsg, "Keeping: %s", szItem);
      WriteChatColor(szMsg,CONCOLOR_GREEN);
   } else {
      sprintf(szMsg, "Destroying: %s", szItem);
      WriteChatColor(szMsg,CONCOLOR_RED);
   }
}

void Load_INI(VOID) {
   char szTemp[MAX_STRING];

   GetPrivateProfileString("General","AutoKeepAll","NULL",szTemp,MAX_STRING,INIFileName);
   if (strstr(szTemp,"NULL")) {
      WriteChatColor("MQ2Forage: INI not found, creating defaults.", CONCOLOR_RED);
      WritePrivateProfileString("General","AutoKeepAll","on",INIFileName);
      WritePrivateProfileString("General","AutoAddAll","on",INIFileName);
   } else {
      GetPrivateProfileString("General","AutoKeepAll","NULL",szTemp,MAX_STRING,INIFileName);
      if ((strcmp(szTemp,"NULL"))||strcmp(szTemp,"on")) {
         AutoKeepEnabled=true;
      } else {
         AutoKeepEnabled=false;
      }
   }

   GetPrivateProfileString("General","AutoAddAll","NULL",szTemp,MAX_STRING,INIFileName);
   if ((strcmp(szTemp,"NULL"))||strcmp(szTemp,"on")) {
      AutoAddEnabled=true;
   } else {
      AutoAddEnabled=false;
   }
}

bool Check_INI(VOID) {

   char szTemp[MAX_STRING];
   char szKeep[MAX_STRING];
   char szItem[64];
   char szZone[64];
   char szMsg1[MAX_STRING];
   bool ItemSetting=false;

   PCHARINFO pChar = GetCharInfo();
   PITEMINFO pCursor = GetItemFromContents(GetCharInfo2()->pInventoryArray->Inventory.Cursor);
   sprintf(szItem,"%s",pCursor->Name);
   sprintf(szZone,"%s",GetFullZone(pChar->zoneId));
   sprintf(szKeep,"%s",AutoKeepEnabled?"keep":"destroy");

   GetPrivateProfileString(szZone,szItem,"NULL",szTemp,MAX_STRING,INIFileName);
   if (strstr(szTemp,"NULL")) {
      if (AutoKeepEnabled) {
         ItemSetting=true;
      }

      if (AutoAddEnabled) {
         WritePrivateProfileString(szZone,szItem,szKeep,INIFileName);
      }
   } else if (strstr(szTemp,"keep")) {
      ItemSetting=true;
   } else if (strstr(szTemp,"destroy")) {
      ItemSetting=false;
   } else {
      sprintf(szMsg1, "MQ2Forage: Bad status in ini for item %s in zone %s. Using global setting.",szItem,szZone);
      WriteChatColor(szMsg1, CONCOLOR_RED);
      if (AutoKeepEnabled) {
         ItemSetting=true;
      }
   }

   return ItemSetting;
}

// added by jaq -- ripped from moveutils

bool IsBardClass() {
   if(strncmp(pEverQuest->GetClassDesc(GetCharInfo2()->Class & 0xff),"Bard",5))
      return false;
   else
      return true;
}